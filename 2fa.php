<?php
// This is a mess
// Not actually very secure
// Should probably create a 2fa token of some kind on server if succesful

/**
 * 2fa.php
 * Handles two-factor auth requests
 * 2fa flow:
 *  1) banking.php redirects to 2fa.php with POST data:
 *      * user_id (required)
 *      * action (required) - name of the action requesting the 2fa 
 *      * return_action (required) - name of the action to return to after succesful auth
 *      * 2fa.php accepts arbitrary POST data that is passed through
 * 
 *  2) 2fa.php first pass - ask user for a 2fa code
 *      * after user enters the code and presses submit, 2fa.php is loaded again
 * 
 *  3) 2fa.php second pass - do stuff and redirect
 *      * if auth is succesful:
 *          * if action is 'login' -> create new session
 *          * redirect to banking.php with return_action as action and POST data intact
 *      * if auth is unsuccesful:
 *          * discard POST data
 *          * redirect to banking.php with 'overview' as action
 * 
 */




date_default_timezone_set("Europe/Helsinki");

require_once('functions.php');
require_once('2fa_functions.php');
require_once('logging.php');
require_once('site_navigation.php');
require_once('snippets.php');
require_once('session.php');

logging(DEBUG, '2fa.php - POST data:');
log_array(DEBUG, $_POST);

// Not sure if all this is needed
if (isset($_POST['user_id'])){ $user_id = intval($_POST['user_id']); }
else { die("Authentication error"); }
if (isset($_POST['list_id'])){ $list_id = intval($_POST['list_id']); }
else { die("Authentication error"); }
if (isset($_POST['list_key'])){ $list_key = intval($_POST['list_key']); }
else { die("Authentication error"); }
if (isset($_POST['2fa_user_input'])){ $user_input = intval($_POST['2fa_user_input']); }
else { $user_input = ""; }
if (isset($_POST['action'])){ $action = $_POST['action']; }
else { die("Authentication error"); }
if (isset($_POST['return_action'])){ $return_action = $_POST['return_action']; }
else { die("Authentication error"); }
if (isset($_POST['ask_user'])){ $ask_user = intval($_POST['ask_user']); }
else {$ask_user = 0; }


if (!$ask_user){
    // Verify 2fa code here
    $conn = connect();
    $auth_result = verify_2fa_code($conn, $user_id, $list_id, $list_key, $user_input);

    if ($auth_result) { 
        logging(DEBUG, "2fa auth valid");
        if ($action == 'login') { add_session($conn, $user_id); }

        // Pass-through POST data
        $data = $_POST;
        $data['action'] = $return_action;
        //log_array("DEBUG", $data);
        redirect_with_post_data('banking.php', $data);
    }
    else { 
        // Discard POST data if unsuccesful
        redirect_with_post_data('banking.php', array('message' => "Väärä vahvistuskoodi", 
                                                    'action' => 'overview')); 
    }
}



// Ask user for 2fa code
else {
    $page_start_snippet = snippet_page_start();
    $page_end_snippet = snippet_page_end();

    $list_key_string = pad_with_zeroes($list_key, 2);

    // Pass through all POST data
    $post_data_hidden_inputs = "";
    foreach ($_POST as $key => $value){
        // ask_user needs to be 0 or we are in infinite loop
        if ($key != 'ask_user'){
            $post_data_hidden_inputs .= "
            <input type='hidden' name='{$key}' value='{$value}' />";
        }
    }
echo "
{$page_start_snippet}        
        <div class='rounded-corners-1'>
            <form name='2fa_form' action='2fa.php' method='post'>
                <h1>Anna vahvistuskoodi kohdasta {$list_key_string}:</h1>
                <input id='focus' type='password' name='2fa_user_input' />
                <input type='hidden' name='ask_user' value='0' />
                {$post_data_hidden_inputs}
                <br />
                <input type='submit' value='Lähetä' />
                <p>Avainlukulistan numero: {$list_id}</p>
            </form>
        </div>
{$page_end_snippet}
";
}


?>