<?php
date_default_timezone_set("Europe/Helsinki");

require_once('user.php');
require_once('functions.php');
require_once('logging.php');

function verify_2fa_code($conn, int $user_id, int $list_id, int $list_key, int $user_input) {
    // Returns true/false

    // 0: Get 2fa list info fron db with given list id
    $query = "SELECT * FROM 2fa_lists WHERE id='{$list_id}'";
    logging(DEBUG, $query);
    $result = mysqli_query($conn, $query);
    $list_info = mysqli_fetch_array($result);

    
    /*
    foreach ($list_info as $key => $value){
        echo "<br>{$key}: {$value}";
    }*/

    // 1: Check that we actually got something in the query
    if ($list_info == null || count($list_info) < 1) { 
        logging("ERROR", "Auth error: no 2fa list with id {$list_id}");
        return false; }
    
    // 2: Check that list belongs to the given user
    if ($list_info['user'] != $user_id) { 
        logging("ERROR", "Auth error: 2fa list {$list_id} doesn't belong to user {$user_id}");
        return false; }

    // 3: Check that user input matches key data
    $key_string = "key_{$list_key}";
    logging(DEBUG, "Keylist value: {$list_info[$key_string]}");
    if (pad_with_zeroes($list_info[$key_string], 4) == $user_input){ return true; }

    // Return false in the end
    return false;
}


function ask_2fa_code($conn, int $user_id, $action, $return_action, array $data){
    // Can pass forward any data 
    $list_id = key_list_id_for_user($conn, $user_id);
    $list_key = rand(1, 20);
    $post_data = array(
        'list_id' => $list_id,
        'list_key' => $list_key,
        'user_id' => $user_id,
        'action' => $action,
        'return_action' => $return_action,
        'ask_user' => 1
    );

    // Add optional data to request
    foreach ($data as $key => $value){
        $post_data[$key] = $value;
    }

    redirect_with_post_data('2fa.php', $post_data);

    // Don't output anything else
    die();
}


function get_2fa_list($conn, int $list_id){
    // Return an array containing all values for given list id
    $query = "SELECT";
    
    for($i = 1; $i <= 20; $i++){
        $query .= " key_{$i}";
        if ($i < 20){ $query .= ","; }
    }

    $query .= " from 2fa_lists WHERE id = '{$list_id}'";

    $result = mysqli_query($conn, $query);
    return mysqli_fetch_array($result);
}

function create_2fa_keys($conn){
    // Create 20 new 2-factor auth keys
    // Save them to database
    // Return keylist id

    $keylist_id = rand();

    $keys_string = "";
    $values_string = "";

    for ($i = 1; $i <= 20; $i++){
        if ($i > 1){ 
            $keys_string .= ", "; 
            $values_string .= ", ";
        }
        $keys_string .= "key_{$i}";

        $random_value = pad_with_zeroes(strval(rand(0, 9999)), 4);
        $values_string .= "'{$random_value}'";
    }
    $query = "INSERT INTO 2fa_lists (id, {$keys_string}) VALUES ('{$keylist_id}', {$values_string})";
    $result = mysqli_query($conn, $query);

    logging(DEBUG, "in create_2fa_keys: query {$query}");

    return $keylist_id;
}


function unused_2fa_lists($conn){
    $query = "SELECT COUNT(*) FROM 2fa_lists WHERE user = '0'";
    $result = mysqli_query($conn, $query);
    return intval(mysqli_fetch_array($result)[0]);
}


function get_unused_2fa_lists($conn, int $number = 9){
    // 1) get id's of unused 2fa lists
    $query = "SELECT * FROM 2fa_lists WHERE user = '0' LIMIT {$number}";
    logging(DEBUG, $query);
    $result = mysqli_query($conn, $query);
    $lists = mysqli_fetch_all($result, MYSQLI_BOTH);

    return $lists;
}


function get_2fa_lists($conn, int $number = 9, bool $unprinted_only = false){
    $query = "SELECT * FROM 2fa_lists";
    if ($unprinted_only){ $query .= " WHERE printed = '0'"; }
    $query .= " LIMIT {$number}";
    logging(DEBUG, $query);
    $result = mysqli_query($conn, $query);
    $lists = mysqli_fetch_all($result, MYSQLI_BOTH);

    return $lists;
}


function set_2fa_list_as_printed($conn, int $list_id){
    $query = "UPDATE 2fa_lists SET printed = '1' WHERE id = '{$list_id}'";
    $result = mysqli_query($conn, $query);
    if (!$result){ 
        logging(ERROR, "Couldn't set 2fa list {$list_id} as printed"); 
        return false;
    }
    return true;
}

?>