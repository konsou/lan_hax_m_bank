<?php
date_default_timezone_set("Europe/Helsinki");

require_once('admin.php');
require_once('logging.php');
require_once('functions.php');
require_once('user.php');

function account_balance($conn, $account_number, int $user_id){
    // Admin can see all account balances
    if (is_admin($conn, $user_id)){
        $query = "SELECT balance FROM accounts WHERE number='{$account_number}'";
    }
    // Not an admin - check that given user actually owns the account
    else {
        $query = "SELECT balance FROM accounts WHERE number='{$account_number}' AND user_id='{$user_id}'";
    }

    // Sanitize
    $account_number = mysqli_real_escape_string($conn, $account_number);

    $result = mysqli_query($conn, $query);
    $data = mysqli_fetch_array($result);

    if ($data == null){ 
        $error_message = "Authentication error - user {$user_id} tried to access balance for account {$account_number}";
        logging("EXCEPTION", $error_message);
        throw new Exception($error_message);
    }

    return $data[0];
}


function account_transactions($conn, int $user_id, string $account_number, int $limit = 50){
    // Return an array of transactions - limit to $limit

    $account_number = mysqli_real_escape_string($conn, $account_number);

    // Check permissions
    if (account_owner($conn, $account_number) == $user_id || is_admin($conn, $user_id)){
        $query = "SELECT * FROM transactions WHERE account_from = '{$account_number}' OR account_to = '{$account_number}' ORDER BY timestamp DESC LIMIT {$limit}";
        $result = mysqli_query($conn, $query);
        $transactions = mysqli_fetch_all($result);
        if ($transactions == null){ return array(); }
        else { return $transactions; }
    }

    // No permissions - return empty array
    logging("ERROR", "User {$user_id} has no permissions to access account {$account_number} transactions");
    return array();
}

function get_accounts($conn, int $user_id){
    // Return an array of accounts the given user can access
    // or empty array if user has no accounts
    if (is_admin($conn, $user_id)){
        // Admin can access all accounts
        $query = "SELECT * FROM accounts";
    }
    else {
        $query = "SELECT * FROM accounts WHERE user_id='{$user_id}'";
    }

    $result = mysqli_query($conn, $query);
    $accounts = mysqli_fetch_all($result);

    if ($accounts == null){ return array(); }
    return $accounts;
}


function account_owner($conn, $account_number){
    // Return the user id who owns the given account number
    // -1 means error
    $account_number = mysqli_real_escape_string($conn, $account_number);
    $query = "SELECT user_id FROM accounts WHERE number='{$account_number}'";
    //logging(DEBUG, "in account_owner - query: {$query}");
    $result = mysqli_query($conn, $query);
    $data = mysqli_fetch_array($result);

    if ($data == null){ return -1; }
    return intval($data[0]);
}


function _account_balance($conn, $account_number){
    // Return the account balance
    // NO PERMISSION CHECK - INTERNAL USE ONLY WHERE PERMISSION HAS ALREADY BEEN CHECKED
    $account_number = mysqli_real_escape_string($conn, $account_number);
    $query = "SELECT balance FROM accounts WHERE number='{$account_number}'";
    logging(DEBUG, "in _account_balance - query: {$query}");
    $result = mysqli_query($conn, $query);
    $data = mysqli_fetch_array($result);

    if ($data == null){ return -1; }
    // All balances use only 2 decimals after comma
    return round(floatval($data[0]), 2);
}


function _add_to_account_balance($conn, $account_number, float $amount){
    // Add $amount to given account's balance
    // Return the new actual account balance
    // Zillions of queries but who cares
    // NO PERMISSION CHECK - INTERNAL USE ONLY WHERE PERMISSION HAS ALREADY BEEN CHECKED
    $current_balance = _account_balance($conn, $account_number);
    $new_balance = $current_balance + $amount;    
    $account_number = mysqli_real_escape_string($conn, $account_number);

    $query = "UPDATE accounts SET balance='{$new_balance}' WHERE number='{$account_number}'";
    if (!mysqli_query($conn, $query)){
        logging("ERROR", "Error updating account balance for account {$account_number}, amount: {$amount}");
        die();
    }

    $actual_final_balance = _account_balance($conn, $account_number);
    return $actual_final_balance;
}


function _add_transaction($conn, $from_account, $to_account, float $amount, $message, $timestamp){
    // Adds a transaction to the transaction log
    // NO PERMISSION CHECK - INTERNAL USE ONLY WHERE PERMISSION HAS ALREADY BEEN CHECKED
    // $timestamp = time();
    $from_account = mysqli_real_escape_string($conn, $from_account);
    $to_account = mysqli_real_escape_string($conn, $to_account);
    $message = mysqli_real_escape_string($conn, $message);
    $timestamp = mysqli_real_escape_string($conn, $timestamp);
    $query = "INSERT INTO transactions (account_from, account_to, amount, message, timestamp) VALUES ('{$from_account}', '{$to_account}', '{$amount}', '{$message}', '{$timestamp}')";
    logging(DEBUG, $query);
    if (!mysqli_query($conn, $query)){
        logging("ERROR", "Error adding transaction: {$amount} from {$from_account} to {$to_account} with message {$message}");    
        die();
    }
    return true;
}


function transfer_money($conn, int $user_id, $from_account, $to_account, float $amount, $message, $timestamp = null){
    // Transfer money from one account to another
    // 0 - only admin can transfer negative amounts or set timestamp freely
    // 1 - check if the user has the right to transfer
    //     (owns from_account or is an admin)
    // 2 - add new transaction
    // 3 - update account balances
    // return true if transfer was succesful or false if it failed
    logging(DEBUG, "in transfer_money - timestamp is {$timestamp}");
    
    // Need to know if user is an admin for following steps
    $user_is_admin = is_admin($conn, $user_id);


    // Don't need precision beyond 2 digits
    $amount = round($amount, 2);

    // 0 - only admin can transfer negative amounts
    if (!$user_is_admin && $amount < 0) { return false; }

    // 0 - Only admin can set timestamp freely
    if (!$user_is_admin && $timestamp != null){ return false; }
    
    // 1 - check if the user has the right to transfer
    if ($user_id != account_owner($conn, $from_account) && !$user_is_admin){
        // Not the owner and not an admin - no right to transfer!
        return false;
    }

    // 2 - add new transaction
    if ($timestamp == null){ $timestamp = time(); }
    _add_transaction($conn, $from_account, $to_account, $amount, $message, $timestamp);
    
    // 3 - update account balances
    // TODO: currently no checking if these succeed or not
    _add_to_account_balance($conn, $from_account, -1 * $amount);
    _add_to_account_balance($conn, $to_account, $amount);


    return true;
}


function account_selection_html($conn, int $user_id, string $select_name, bool $show_name = false){
    $accounts = get_accounts($conn, $user_id);

    $account_selection_input = "<select name='{$select_name}'>";
    foreach ($accounts as $account){
        $account_number = $account[0];

        if ($show_name){
            $account_owner_id = account_owner($conn, $account_number);
            $account_owner_name = user_full_name($conn, $account_owner_id);
            $account_owner_string = "{$account_owner_name} | ";
        }
        else { $account_owner_string = ""; }
        $account_balance = nice_currency_format_string($account[2]);

        $account_selection_input .= "
        <option value='{$account_number}'>{$account_number} | {$account_owner_string}{$account_balance}</option>";
    }
    $account_selection_input .= "</select>";

    return $account_selection_input;

}
?>