<?php
date_default_timezone_set("Europe/Helsinki");

require_once('functions.php');
require_once('2fa_functions.php');
require_once('snippets.php');
require_once('admin.php');
require_once('account.php');
require_once('logging.php');
require_once('session.php');
require_once('site_navigation.php');

require_once('view_payment.php');
require_once('view_overview.php');
require_once('view_account_info.php');
require_once('view_2fa_list.php');
require_once('view_admin_page.php');
require_once('view_user_info.php');
require_once('view_user_management.php');


// Start connection
$conn = connect();

// Info message
$message = "";

// Debugging
logging(DEBUG, "POST data:");
log_array(DEBUG, $_POST);
// logging(DEBUG, "GET data:");
// log_array(DEBUG, $_GET);
//print_array($_POST);


////////////////////////
// Sanitize POST data //
////////////////////////

// we don't like GET - ugly URL's
// Actually using POST seems a bad idea in retrospect - refreshing page is a problem
// Well, what can you do!

// Don't trust POST data to set user ID! Fetch from database using session id instead.
if (isset($_POST['login_form_user_id'])){ $login_form_user_id = intval($_POST['login_form_user_id']); }
else { $login_form_user_id = 0; }
if (isset($_POST['login_form_password'])){ $login_form_password = intval($_POST['login_form_password']); }
else { $login_form_password = 0; }
if (isset($_POST['list_id'])){ $list_id = intval($_POST['list_id']); }
else { $list_id = 0; }
if (isset($_POST['action'])){ $action = $_POST['action']; }
else { $action = ""; }
if (isset($_POST['account_number'])){ $account_number = $_POST['account_number']; }
else { $account_number = ""; }
if (isset($_POST['account_from'])){ $account_from = $_POST['account_from']; }
else {$account_from = ""; }
if (isset($_POST['account_to'])){ $account_to = $_POST['account_to']; }
else {$account_to = ""; }
if (isset($_POST['amount'])){ $amount = floatval(str_replace(",", ".", $_POST['amount']));}
else {$amount = 0; }
if (isset($_POST['payment_message'])){ $payment_message = $_POST['payment_message'];}
else {$payment_message = "";}
if (isset($_POST['payment_timestamp'])){ $payment_timestamp = strtotime($_POST['payment_timestamp']); }
else {$payment_timestamp = 0;}

$paytime = date(DATE_RSS, $payment_timestamp);
//logging(DEBUG, "Payment timestamp is {$payment_timestamp} - $paytime");
logging(DEBUG, "Action is {$action}");


// Set login failed info message if applicable
// Messages shouldn't be passed using POST - can be manipulated
if (isset($_POST['message'])){ $message = strip_tags($_POST['message']); }

///////////////////////////
// Check for valid login //
///////////////////////////

// Default value is false
$login = false;

// Read session id from cookie
if (isset($_COOKIE['m_bank_session_id'])){ $session_id = intval($_COOKIE['m_bank_session_id']); }
else { $session_id = 0; }

// Get user id from session
$user_id = get_user_for_session($conn, $session_id);

// First check if we have a valid session already
if (valid_session_active($conn, $user_id)){ 
    $login = true; 
}
// If no valid session check username and password
else {
    logging(INFO, "No valid session - login needed");
    if (check_login_password($conn, $login_form_user_id, $login_form_password)){
        // If username and password ok check 2fa
        // This will take us to another page and reload this page afterwards
        ask_2fa_code($conn, $login_form_user_id, 'login', 'overview', array());
    }
}

///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////
// PAGE CONSTRUCTION STARTS HERE //
///////////////////////////////////


////////////////////////
// NAVIGATION BUTTONS //
////////////////////////

// Navigation buttons are currently implemented using a js hack that
// redirects with POST data

$navigation_buttons = "";

// Navigation buttons shown only if user is logged in
if ($login) {
    // Show button to enter admin area if user is an admin
    if (is_admin($conn, $user_id)){ 
        $navigation_buttons .= navigation_button('admin', 'Hallinta'); 
        $navigation_buttons .= navigation_button('user_management', 'Käyttäjät');
    }

    $navigation_buttons .= tabs(3) . navigation_button('overview', 'Etusivu');
    $navigation_buttons .= tabs(3) . navigation_button('payment', 'Uusi maksu');
    $navigation_buttons .= tabs(3) . navigation_button('user_info', 'Omat tiedot');
    $navigation_buttons .= tabs(3) . navigation_button('logout', 'Kirjaudu ulos');
}


//////////////
// SNIPPETS //
//////////////
$page_start_snippet = snippet_page_start();
$login_form_snippet = snippet_login_form();
$page_end_snippet = snippet_page_end();


///////////////////////
// ACTION SELECTION  //
// Sets page content //
///////////////////////
// NEEDS MAJOR OVERHAUL - IS UGLY AS BUTT

$page_content = "";

if ($login) {
    if ($action == 'admin'){
        // admin panel stuff
        if (is_admin($conn, $user_id)){ $page_content = construct_admin_page($conn, $user_id); }
        else { $page_content = "Et ole järjestelmänvalvoja. Hus pois täältä."; }
    }
    elseif ($action == 'admin_add_transactions'){
        if (is_admin($conn, $user_id)){ 
            $transfer_info = null;
            if ($account_from != "" && $account_to != "" && $payment_timestamp != 0){
                if (transfer_money($conn, $user_id, $account_from, $account_to, $amount, $payment_message, $payment_timestamp)){
                    $transfer_info = array($payment_timestamp, $account_from, $account_to, $amount, $payment_message);
                }
            }

            $page_content = construct_admin_add_transaction_page($conn, $user_id, $transfer_info); 
        }
        else { $page_content = "Et ole järjestelmänvalvoja. Hus pois täältä."; }
    }
    elseif ($action == 'user_management'){
        // Need consistency as to where admin check takes place! Now in many layers
        if (is_admin($conn, $user_id)) { $page_content = construct_user_management_page($conn, $user_id); }
        else { $page_content = "Et ole järjestelmänvalvoja. Hus pois täältä."; }
    }

    elseif ($action == 'payment'){
        $page_content = construct_payment_page($conn, $user_id);
    }
    elseif ($action == 'transaction_init'){
        // 2fa will reload page after finished
        ask_2fa_code($conn, $user_id, $action, 'transaction_finalize', $_POST);
    }
    elseif ($action == 'transaction_finalize'){
        // Change "," to "." in decimal numbers
        $amount = floatval(str_replace(",", ".", $_POST['amount']));
        $transfer_success = transfer_money($conn, $user_id, $_POST['account_from'], $_POST['account_to'], $amount, $_POST['payment_message']);
        $page_content = construct_payment_finished_page($conn, $user_id, $_POST, $transfer_success);
    }
    elseif ($action == 'account_info'){
        $page_content = construct_account_info_page($conn, $user_id, $account_number);
    }
    elseif ($action == 'view_2fa_list'){
        $page_content = construct_2fa_list_page($conn, $user_id, $list_id);
    }
    elseif ($action == 'create_2fa_codes'){
        if (is_admin($conn, $user_id)){
            // Create 9 new 2fa lists
            for ($i = 0; $i < 9; $i++){
                create_2fa_keys($conn);
            }
        }
        redirect_with_post_data("banking.php", array("action" => "admin", "user_id" => $user_id));
    }
    elseif ($action == 'user_info'){
        $page_content = construct_user_info_page($conn, $user_id);
    }
    elseif ($action == 'logout'){
        logout($conn, $session_id);
        redirect_with_post_data('banking.php', array('message' => "Olet nyt kirjautunut ulos verkkopankista."));
    }
    else { // overview is the default action if logged in
        // do overview stuff
        $page_content = construct_overview_page($conn, $user_id);
    }
}
else { // no login - show only login form
    $page_content = "
    <h2>Kirjaudu sisään</h2>
    {$login_form_snippet}
    ";
}


// Output
echo "
{$page_start_snippet}
            <!-- <h1>Verkkopankki</h1> -->
            <h2>{$message}</h2>
            <div id='navigation-buttons'>{$navigation_buttons}
            </div>
            {$page_content}
{$page_end_snippet}
";

mysqli_close($conn);
?>