<?php
date_default_timezone_set("Europe/Helsinki");

// TODO: MOVE EVERYTHING HERE TO BETTER NAMED FILES

require_once('2fa_functions.php');
require_once('logging.php');

function connect(){
    $dbhost = 'localhost';
    $dbuser = 'admin';
    $dbpass = 'haeskellakehitysmaaravithipihiljaa';
    $dbname = 'm_bank';

    $conn = mysqli_connect($dbhost, $dbuser, $dbpass) or die ('Error connecting to mysql');
    mysqli_set_charset($conn, 'utf8');
    $database = mysqli_select_db($conn, $dbname);

    return $conn;
}


function check_login_password($conn, int $user_id, int $password){
    // Just checks if given user's password is correct and returns true/false 
    
    logging(DEBUG, "check_login_password for user {$user_id}");

    // Query database
    $query = "SELECT COUNT(*) FROM users WHERE id='{$user_id}' AND password='{$password}'";
    logging(DEBUG, $query);
    $result = mysqli_query($conn, $query);
    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);

    if ($row['COUNT(*)'] == 1){ return true; }
    return false;
}



function pad_with_zeroes($input_value, int $length){
    $input_value = strval($input_value);
    while (strlen($input_value) < $length){
        $input_value = "0{$input_value}";
    }
    return $input_value;
}


function tabs(int $number){
    $ret_val = "";
    for ($i = 0; $i < $number; $i++){
        $ret_val .= "    ";
    }
    return $ret_val;
}

function nice_currency_format_html($number){
    // Return a nice currency string with <span> tag and style attribute according to positivity/negativity
    $number_string = nice_currency_format_string($number);
    if ($number < 0){ $ret_val = "<span class='negative-amount'>{$number_string}</span>"; }
    else { $ret_val = "<span class='positive-amount contains-number'>{$number_string}</span>"; }
    return $ret_val;
}

function nice_currency_format_string(float $number){
    // Return a nice currency string starting with +/-, ending with " €" and properly formatted
    // Decimal separator is comma ","
    // Thousands separator is space " "
    $number_string = number_format($number, 2, ",", " ");
    if ($number < 0) { $ret_val = "- {$number_string} €"; }
    else { $ret_val = "+ {$number_string} €"; }
    return $ret_val;
}


function timestamp_to_date(int $timestamp){
    return date("j.n.Y H:i", $timestamp);
}
?>