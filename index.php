<?php
date_default_timezone_set("Europe/Helsinki");

require_once('snippets.php');

// Logic stuff here
$page_start_snippet = snippet_page_start();
$login_form_snippet = snippet_login_form();
$page_end_snippet = snippet_page_end();




// Output here
echo "
{$page_start_snippet}
        
        <h1>Tervetuloa M-Pankkiin!</h1>
        <!-- <img src='img/logo_mbank_64.png' /> -->
        <h2>Olemme täällä sinua varten</h2>
        <h3>Kirjaudu verkkopankkiin tästä:</h3>
        {$login_form_snippet}

{$page_end_snippet}
";

?>
