<?php
date_default_timezone_set("Europe/Helsinki");

define("DEBUG", 0);
define("INFO", 1);
define("WARNING", 2);
define("ERROR", 3);
define("EXCEPTION", 4);

// CHANGE THIS TO CHANGE LOGGING LEVEL
define("LOGGING_LEVEL", WARNING);

$SILENCE_LOGGING = false;

$LOGGING_LEVEL_TO_STRING = array(
    0 => "DEBUG",
    1 => "INFO",
    2 => "WARNING",
    3 => "ERROR",
    4 => "EXCEPTION"
);


function logging(int $level, string $message){
    global $LOGGING_LEVEL_TO_STRING;
    global $SILENCE_LOGGING;

    if ($level >= LOGGING_LEVEL){
        $level_as_string = $LOGGING_LEVEL_TO_STRING[$level];

        if (ini_get("display_errors") && !$SILENCE_LOGGING) { echo "<br>[{$level_as_string}] {$message}"; }
        error_log("[{$level_as_string}] {$message}");
        }
}


function log_array(int $level, array $array){
    foreach ($array as $key => $value){
        logging($level, "{$key}: {$value}");
    }
}

function silence_logging(bool $silence = true){
    global $SILENCE_LOGGING;
    $SILENCE_LOGGING = $silence;
}
?>