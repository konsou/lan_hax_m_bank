-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 24.11.2018 klo 18:54
-- Palvelimen versio: 10.1.34-MariaDB-0ubuntu0.18.04.1
-- PHP Version: 7.2.10-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `m_bank`
--

-- --------------------------------------------------------

--
-- Rakenne taululle `2fa_lists`
--

CREATE TABLE `2fa_lists` (
  `id` bigint(20) NOT NULL,
  `user` bigint(20) NOT NULL DEFAULT '0',
  `key_1` int(11) NOT NULL,
  `key_2` int(11) NOT NULL,
  `key_3` int(11) NOT NULL,
  `key_4` int(11) NOT NULL,
  `key_5` int(11) NOT NULL,
  `key_6` int(11) NOT NULL,
  `key_7` int(11) NOT NULL,
  `key_8` int(11) NOT NULL,
  `key_9` int(11) NOT NULL,
  `key_10` int(11) NOT NULL,
  `key_11` int(11) NOT NULL,
  `key_12` int(11) NOT NULL,
  `key_13` int(11) NOT NULL,
  `key_14` int(11) NOT NULL,
  `key_15` int(11) NOT NULL,
  `key_16` int(11) NOT NULL,
  `key_17` int(11) NOT NULL,
  `key_18` int(11) NOT NULL,
  `key_19` int(11) NOT NULL,
  `key_20` int(11) NOT NULL,
  `printed` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Vedos taulusta `2fa_lists`
--

INSERT INTO `2fa_lists` (`id`, `user`, `key_1`, `key_2`, `key_3`, `key_4`, `key_5`, `key_6`, `key_7`, `key_8`, `key_9`, `key_10`, `key_11`, `key_12`, `key_13`, `key_14`, `key_15`, `key_16`, `key_17`, `key_18`, `key_19`, `key_20`, `printed`) VALUES
(348729569, 0, 3804, 4863, 3551, 8989, 8500, 4683, 1125, 5895, 4470, 6283, 4404, 4828, 6047, 669, 3232, 2076, 8082, 5509, 7210, 8944, 1),
(410662022, 55468034, 6963, 9032, 9574, 6789, 5750, 4089, 7785, 6634, 9917, 3589, 7498, 828, 6314, 8757, 2524, 5824, 5492, 4554, 5716, 3332, 1),
(481492054, 63545614, 3448, 8967, 6187, 3812, 9158, 6788, 1453, 7510, 6400, 2483, 5112, 6528, 5429, 6902, 7677, 9335, 7387, 7350, 3101, 1592, 1),
(518916233, 0, 577, 5858, 2395, 4703, 5165, 1639, 5893, 1517, 3431, 8462, 7716, 1687, 4032, 1001, 1249, 9706, 7141, 9009, 3644, 1191, 1),
(596727411, 0, 5257, 739, 8170, 4810, 6146, 6174, 4820, 2176, 7059, 27, 840, 6033, 6750, 320, 7819, 6142, 9775, 2376, 1735, 3984, 1),
(668772978, 12345678, 1159, 7084, 7424, 6303, 6231, 4316, 4759, 5787, 9132, 2812, 3, 7923, 4254, 931, 8830, 7107, 3210, 2481, 3968, 3774, 1),
(779681131, 83750021, 9429, 2685, 6331, 9237, 8489, 4246, 3219, 156, 292, 3484, 8521, 2853, 5117, 2489, 5025, 648, 302, 1467, 1326, 218, 1),
(876181040, 13839274, 447, 8151, 4686, 9465, 1680, 1355, 707, 6629, 7037, 7816, 6613, 2284, 5576, 6433, 918, 1778, 4792, 2288, 4683, 1690, 1),
(1119659245, 0, 6119, 4950, 1393, 9170, 1676, 7314, 6761, 1082, 8443, 8628, 6113, 4457, 546, 1107, 9271, 6752, 5505, 8814, 4665, 4510, 1),
(1152060528, 20955871, 242, 9727, 8313, 9604, 8642, 2483, 3369, 5672, 5364, 6129, 7179, 6623, 7640, 7922, 7110, 9536, 650, 8260, 7023, 7905, 1),
(1208398822, 69726475, 7565, 9583, 1239, 1386, 5236, 9842, 1205, 3985, 8296, 2704, 5680, 429, 1571, 5957, 7080, 1627, 2113, 1119, 5971, 6808, 1),
(1449977453, 30203833, 4827, 8333, 31, 583, 1912, 2399, 1017, 7435, 8604, 4191, 424, 2956, 5766, 2166, 7431, 2090, 4123, 4274, 267, 2731, 1),
(1487184737, 31914797, 5861, 6686, 317, 3020, 9393, 9217, 130, 2909, 1623, 5493, 6274, 3869, 9137, 1428, 5182, 6815, 4634, 7024, 6831, 6145, 1),
(1523652851, 0, 3685, 9322, 4460, 3392, 4366, 6260, 9900, 3459, 4484, 43, 5632, 45, 1737, 6114, 337, 8715, 1256, 9307, 1871, 4599, 1),
(1576719265, 0, 6778, 600, 1772, 2147, 9437, 4521, 2825, 2142, 5850, 8202, 7768, 6301, 5306, 5561, 9682, 4834, 1749, 5515, 2269, 5136, 1),
(1816373168, 0, 7773, 4013, 8481, 4389, 9825, 3342, 6393, 4184, 3781, 1678, 3032, 1314, 1735, 1488, 4083, 6915, 9603, 1322, 6580, 937, 1),
(1828998829, 0, 566, 9491, 8056, 9397, 2744, 7189, 4731, 3440, 3746, 8252, 4683, 835, 3732, 418, 1516, 2252, 3124, 3693, 7795, 456, 1),
(1875434281, 0, 482, 1598, 6233, 6502, 9560, 1271, 5619, 1785, 4292, 4020, 3323, 4053, 8123, 5593, 7570, 9022, 9384, 8508, 7363, 4792, 1),
(1939159415, 0, 1357, 7987, 6955, 130, 1942, 1115, 5769, 3833, 2445, 6486, 5217, 5390, 4401, 3146, 9405, 5036, 2255, 3218, 9395, 4590, 0),
(2103305038, 0, 5645, 5390, 972, 9146, 5700, 8346, 6165, 7451, 2743, 7859, 5898, 1833, 3644, 7020, 5552, 145, 5796, 7080, 3629, 7051, 0);

-- --------------------------------------------------------

--
-- Rakenne taululle `accounts`
--

CREATE TABLE `accounts` (
  `number` varchar(10) NOT NULL,
  `user_id` int(11) NOT NULL,
  `balance` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Vedos taulusta `accounts`
--

INSERT INTO `accounts` (`number`, `user_id`, `balance`) VALUES
('MP13371179', 13839274, 100),
('MP13371337', 12345678, 963520.76),
('MP13372230', 20955871, 100),
('MP13372389', 17747205, 154239.03),
('MP13372945', 83750021, 224142.75),
('MP13373019', 30203833, 100),
('MP13373429', 82710096, 79231.34),
('MP13374294', 31914797, 100),
('MP13374412', 55468034, 100),
('MP13375098', 33756139, 348.21),
('MP13375523', 63545614, 100),
('MP13375853', 69726475, 100),
('MP13375973', 97010425, 100),
('MP13379133', 0, 100),
('MP13379536', 0, 100);

-- --------------------------------------------------------

--
-- Rakenne taululle `sessions`
--

CREATE TABLE `sessions` (
  `id` bigint(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Vedos taulusta `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `active`) VALUES
(1, 0, 1),
(39729063, 12345678, 1),
(53257554, 0, 0),
(259150838, 0, 0),
(263764264, 12345678, 1),
(326953998, 12345678, 1),
(417376193, 0, 0),
(496977821, 12345678, 1),
(628350072, 12345678, 0),
(631341437, 12345678, 1),
(663030087, 12345678, 1),
(708080993, 83750021, 1),
(754647227, 0, 1),
(778692718, 0, 0),
(897974878, 12345678, 1),
(905800319, 12345678, 1),
(911996690, 12345678, 1),
(982034009, 0, 0),
(1038618288, 12345678, 1),
(1093156506, 0, 0),
(1203491765, 12345678, 1),
(1245569193, 0, 0),
(1250808521, 0, 0),
(1388400242, 0, 0),
(1423035663, 12345678, 0),
(1532137291, 12345678, 1),
(1561518969, 0, 0),
(1721391112, 12345678, 1),
(1722459129, 12345678, 1),
(1884522035, 0, 0),
(1989912123, 0, 0);

-- --------------------------------------------------------

--
-- Rakenne taululle `transactions`
--

CREATE TABLE `transactions` (
  `id` bigint(20) NOT NULL,
  `account_from` varchar(10) NOT NULL,
  `account_to` varchar(10) NOT NULL,
  `amount` double NOT NULL DEFAULT '0',
  `message` varchar(255) DEFAULT '""',
  `timestamp` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Vedos taulusta `transactions`
--

INSERT INTO `transactions` (`id`, `account_from`, `account_to`, `amount`, `message`, `timestamp`) VALUES
(1, 'MP13371337', 'MP13372945', 5600, 'Eläke', 1541023200),
(2, 'MP13371337', 'MP13372945', 5600, 'Eläke', 1538341200),
(3, 'MP13371337', 'MP13371337', 5600, 'Eläke', 1535749200),
(4, 'MP13371337', 'MP13372945', 5600, 'Eläke', 1535749200);

-- --------------------------------------------------------

--
-- Rakenne taululle `users`
--

CREATE TABLE `users` (
  `id` int(8) NOT NULL,
  `password` int(4) NOT NULL,
  `is_admin` tinyint(4) NOT NULL DEFAULT '0',
  `display_name` text NOT NULL,
  `phone_number` varchar(15) DEFAULT NULL,
  `email` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Vedos taulusta `users`
--

INSERT INTO `users` (`id`, `password`, `is_admin`, `display_name`, `phone_number`, `email`) VALUES
(12345678, 1337, 1, 'admin', '0415048668', 'tomi.javanainen@gmail.com'),
(13839274, 1847, 0, 'Mikko Olkkonen', '0456331918', 'mikko85olkkonen@gmail.com'),
(17747205, 6200, 0, 'Maire Mähönen', '0458085578', 'reeta.javanainen@gmail.com'),
(20955871, 6400, 0, 'Johan Kiviniemi', '0442865530', 'johan@kiviniemi.name'),
(30203833, 3131, 0, 'Toni Kärkkäinen', '0405566865', 'toni.karkkainen@elisanet.fi'),
(31914797, 2443, 0, 'Teemu Siltanen', '0442837840', 'teemu.siltanen@gmail.com'),
(33756139, 5021, 0, 'Sebastian Mummelín', '0415048668', 'tomi.javanainen@gmail.com'),
(55468034, 3112, 0, 'Tuomo Siltanen', '0442840687', 'tuomo.siltanen@gmail.com'),
(63545614, 9007, 0, 'Konsta Kaukonen', '0445297344', 'ke.kaukonen@gmail.com'),
(69726475, 6132, 0, 'Petri Kymäläinen', '0442882285', 'PeraSpede@gmail.com'),
(82710096, 3320, 0, 'Riikka Mummelín', '0451274176', 'roosa.javanainen@gmail.com'),
(83750021, 8320, 0, 'Salme Mummelín', '0414796356', 'salme.mummelin@luukku.com'),
(97010425, 5535, 0, 'Roope Pelttari', '0505601768', 'phantaloz@gmail.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `2fa_lists`
--
ALTER TABLE `2fa_lists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`number`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `2fa_lists`
--
ALTER TABLE `2fa_lists`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2103305039;

--
-- AUTO_INCREMENT for table `sessions`
--
ALTER TABLE `sessions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1989912124;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
