<?php
// PDF - nice backgrounds for 2fa lists

date_default_timezone_set("Europe/Helsinki");

require_once('functions.php');
require_once('2fa_functions.php');
require_once('admin.php');
require_once('session.php');
require_once('fpdf/fpdf.php');
require_once('pdf_rounded_rect.php');

$conn = connect();

// Printing logging messages prevents pdf output - silence logging messages
silence_logging();

////////////////////////////
// CHECK LOGIN AND RIGHTS //
////////////////////////////

// 1) must have a session cookie
if (!isset($_COOKIE['m_bank_session_id'])){ die("Kirjaudu sisään"); }
else { $session_id = $_COOKIE['m_bank_session_id']; }

// 2) session must be valid
$login = session_valid($conn, $session_id);
if (!$login){ die("Kirjaudu sisään"); }

////////////////////////////
// PDF PAGE CREATION HERE //
////////////////////////////

// ALL UNITS MM
$background_r = 255;
$background_g = 255;
$background_b = 255;

$card_width = 85.60;
$card_height = 53.98;
$card_corner_radius = 3;
$card_margin = 2.5;
$card_padding = 3;

$page_width = 297; // A4 landscape
$page_height = 210; // A4 landscape
$page_min_margin = 10;

$cards_in_x_direction = intval(($page_width - 2 * $page_min_margin) / ($card_width + 2 * $card_margin));
$cards_in_y_direction = intval(($page_height - 2 * $page_min_margin) / ($card_height + 2 * $card_margin));

$all_cards_width = $cards_in_x_direction * (2 * $card_margin + $card_width);
$all_cards_height = $cards_in_y_direction * (2 * $card_margin + $card_height);
$page_margin_x = ($page_width - $all_cards_width) / 2;
$page_margin_y = ($page_height - $all_cards_height) / 2;

$image_file = 'img/logo_mbank_2048.png';
$image_width = 30;
$image_height = 30;

$font = "Arial";

$pdf=new PDF('L','mm', array($page_width, $page_height));
$pdf->AddPage();
$pdf->SetAutoPageBreak(FALSE);
$pdf->SetFont($font, '', 16);
$pdf->SetFillColor($background_r, $background_g, $background_b);

// Center lines for alignment help

//$pdf->Line(0, $page_height / 2, $page_width, $page_height / 2);
//$pdf->Line($page_width / 2, 0, $page_width / 2, $page_height);


$card_number = 0;
for ($x = 0; $x < $cards_in_x_direction; $x++){
    for ($y = 0; $y < $cards_in_y_direction; $y++){
        $topleft_x = $page_margin_x + $card_margin + ($x * 2 * $card_margin) + ($x * $card_width);
        $topleft_y = $page_margin_y + $card_margin + ($y * 2 * $card_margin) + ($y * $card_height);
        $allowed_area_width = $card_width - 2 * $card_padding;
        $allowed_area_height = $card_height - 2 * $card_padding;

        $image_x = $topleft_x + $card_padding + $allowed_area_width / 2 - $image_width / 2;
        $image_y = $topleft_y + $card_padding + $allowed_area_height / 2 - $image_height / 2;

        // Card border
        $pdf->RoundedRect($topleft_x, $topleft_y, $card_width, $card_height, $card_corner_radius, '1234', 'DF');

        $pdf->Image($image_file, $image_x, $topleft_y + 10, $image_width, $image_height);

        $card_number++;
    }
}

$pdf->Output();


mysqli_close($conn);
?>