<?php
// Shows unused 2fa lists on a printer-friendly page
// Also marks them as printed
// SO DON'T REFRESH THE PAGE TOO MUCH!

date_default_timezone_set("Europe/Helsinki");

require_once('functions.php');
require_once('2fa_functions.php');
require_once('admin.php');
require_once('session.php');
require_once('fpdf/fpdf.php');
require_once('pdf_rounded_rect.php');


$conn = connect();

// Printing logging messages prevents pdf output - silence logging messages
silence_logging();

////////////////////////////
// CHECK LOGIN AND RIGHTS //
////////////////////////////

// Require admin rights to show this page
// 1) must have a session cookie
if (!isset($_COOKIE['m_bank_session_id'])){ die("Kirjaudu sisään"); }
else { $session_id = $_COOKIE['m_bank_session_id']; }

// 2) session must be valid
$login = session_valid($conn, $session_id);
if (!$login){ die("Kirjaudu sisään"); }

// 3) user must be admin
$user_id = get_user_for_session($conn, $session_id);
if (!is_admin($conn, $user_id)){ die("Et ole järjestelmänvalvoja"); }

////////////////
// FETCH DATA //
////////////////
$lists = get_2fa_lists($conn, 9, true);
/*
foreach ($lists as $value){
    log_array("ERROR", $value);
}
*/


////////////////////////////
// PDF PAGE CREATION HERE //
////////////////////////////

// ALL UNITS MM
$card_width = 85.60;
$card_height = 53.98;
$card_corner_radius = 3;
$card_margin = 2.5;
$card_padding = 3;

$page_width = 297; // A4 landscape
$page_height = 210; // A4 landscape
$page_min_margin = 10;

$cards_in_x_direction = intval(($page_width - 2 * $page_min_margin) / ($card_width + 2 * $card_margin));
$cards_in_y_direction = intval(($page_height - 2 * $page_min_margin) / ($card_height + 2 * $card_margin));

$all_cards_width = $cards_in_x_direction * (2 * $card_margin + $card_width);
$all_cards_height = $cards_in_y_direction * (2 * $card_margin + $card_height);
$page_margin_x = ($page_width - $all_cards_width) / 2;
$page_margin_y = ($page_height - $all_cards_height) / 2;

$font = "Arial";

$pdf=new PDF('L','mm', array($page_width, $page_height));
$pdf->AddPage();
$pdf->SetAutoPageBreak(FALSE);
$pdf->SetFont($font, '', 16);

// Center lines for alignment help
/*
$pdf->Line(0, $page_height / 2, $page_width, $page_height / 2);
$pdf->Line($page_width / 2, 0, $page_width / 2, $page_height);
*/

$card_number = 0;
for ($x = 0; $x < $cards_in_x_direction; $x++){
    for ($y = 0; $y < $cards_in_y_direction; $y++){
        $topleft_x = $page_margin_x + $card_margin + ($x * 2 * $card_margin) + ($x * $card_width);
        $topleft_y = $page_margin_y + $card_margin + ($y * 2 * $card_margin) + ($y * $card_height);
        $allowed_area_width = $card_width - 2 * $card_padding;
        $allowed_area_height = $card_height - 2 * $card_padding;

        $list_id_text_portion_height = 7;
        $list_id_text_size = 14;
        $list_key_line_height = 5;
        $list_key_text_size = 12;
        $key_rows = 5;
        $key_columns = 4;
        $key_y_margin = 5;
        $key_column_width = $allowed_area_width / $key_columns;
        $key_row_height = ($allowed_area_height - $list_id_text_portion_height - 2 * $key_y_margin) / $key_rows;

        // Card border
        $pdf->RoundedRect($topleft_x, $topleft_y, $card_width, $card_height, $card_corner_radius);
        
        // List ID number
        $pdf->SetFont($font, 'B', $list_id_text_size);
        $pdf->SetXY($topleft_x + $card_padding, $topleft_y + $card_padding);
        $key_list_number_string = "{$lists[$card_number]['id']}";
        $pdf->MultiCell($allowed_area_width, $list_id_text_portion_height, $key_list_number_string);

        // Key table
        $pdf->SetFont($font, '', $list_key_text_size);

        for ($row = 1; $row <= $key_rows; $row++){
            for ($column = 1; $column <= $key_columns; $column++){
                // Alternating background colors
                // Odd rows
                if ($row % 2 == 1){ $fill_gray_value = 150; }
                // Even rows
                else {$fill_gray_value = 255; }
                $pdf->SetFillColor($fill_gray_value);

                $current_x = $topleft_x + $card_padding + ($column - 1) * $key_column_width;
                $current_y = $topleft_y + $card_padding + $list_id_text_portion_height + $key_y_margin + ($row - 1) * $key_row_height;

                $list_key = $row + ($key_rows * ($column - 1));
    
                $key_formatted = pad_with_zeroes($list_key, 2);
                $value_formatted = pad_with_zeroes($lists[$card_number][$list_key + 1], 4);

                $pdf->SetXY($current_x, $current_y);
                $pdf->Cell($key_column_width, $key_row_height, "{$key_formatted}: {$value_formatted}", 0, 0, '', true);
                //$list_key++;
            }
        }

        $card_number++;
    }
}


$pdf->Output();

// Mark shown lists as printed 
foreach ($lists as $list){
    set_2fa_list_as_printed($conn, $list['id']);
}


mysqli_close($conn);
?>