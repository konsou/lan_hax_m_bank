<?php
// Show client welcome letter as a PDF page

date_default_timezone_set("Europe/Helsinki");

require_once('user.php');
require_once('account.php');
require_once('functions.php');
require_once('2fa_functions.php');
require_once('admin.php');
require_once('session.php');
require_once('fpdf/fpdf.php');
require_once('pdf_rounded_rect.php');


$conn = connect();

// Printing logging messages prevents pdf output - silence logging messages
silence_logging();

////////////////////////////
// CHECK LOGIN AND RIGHTS //
////////////////////////////

// Require admin rights to show this page
// 1) must have a session cookie
if (!isset($_COOKIE['m_bank_session_id'])){ die("Kirjaudu sisään"); }
else { $session_id = $_COOKIE['m_bank_session_id']; }

// 2) session must be valid
$login = session_valid($conn, $session_id);
if (!$login){ die("Kirjaudu sisään"); }

// 3) user must be admin
$user_id = get_user_for_session($conn, $session_id);
if (!is_admin($conn, $user_id)){ die("Et ole järjestelmänvalvoja"); }

////////////////
// FETCH DATA //
////////////////

$user_id = intval($_GET['user_id']);
$user = user_full_info($conn, $user_id);
// Fpdf needs utf8_decode to display umlauts correctly
$user_name = utf8_decode($user['display_name']);
$account_number = get_accounts($conn, $user_id)[0][0];

logging(INFO, "Print welcome letter for user {$user_id} - {$user['display_name']}");


////////////////////////////
// PDF PAGE CREATION HERE //
////////////////////////////

// ALL UNITS MM
$page_width = 210; // A4 
$page_height = 297; // A4
$page_min_margin = 10;

$font = "Courier";
$font_size = 14;

$logo_image = "img/logo_mbank_with_text_1000.png";
$logo_width = 30;
$logo_height = 30;
$logo_x_position = 150;
$logo_y_position = 20;

$customer_name_part_text = "{$user_name}\n{$user_id}";
$customer_name_part_x = 0;
$customer_name_part_y = 25;
$customer_name_part_width = 100;
$customer_name_part_font_size = 18;
$customer_name_part_row_height = $customer_name_part_font_size / 2.5;
$customer_name_part_align = 'C';

$paragraph_1_text = utf8_decode("Tervetuloa asiakkaaksi M-Pankkiin!

Tässä järjestelmässämme olevat tietomme sinusta:

    Nimi:           {$user['display_name']}
    Puhelinnumero:  {$user['phone_number']}
    Sähköposti:     {$user['email']}

Otathan yhteyttä asiakaspalveluumme, jos ylläolevissa tiedoissa on korjattavaa. Asiakaspalvelumme tavoitat sähköpostilla osoitteesta asiakaspalvelu@m-pankki.ml.

Pääset kirjautumaan verkkopankkiin osoitteessa https://m-pankki.ml

    Verkkopankin asiakasnumerosi on:
    {$user_id}

    Uusi tilinumerosi on:
    {$account_number}

Verkkopankin salasanasi toimitetaan sinulle erikseen. 

HUOM! Jos verkkopankin salasanan pakkaus on avattu niin älä ota sitä käyttöön. Ota tällöin välittömästi yhteyttä asiakaspalveluumme.

Kampanjatarjouksena olemme lisänneet uudelle tilillesi 100 euroa käyttövaraa. Olemmehan täällä sinua varten!

                Ystävällisin terveisin,
                        M-Pankki

P.S. Seuraa M-Pankkia diasporassa: @m_pankki");
$paragraph_1_x = 15;
$paragraph_1_y = 75;
$paragraph_1_width = 180;
$paragraph_1_font_size = 18;
$paragraph_1_row_height = $paragraph_1_font_size / 3;
$paragraph_1_align = 'L';


$pdf=new PDF('P','mm', array($page_width, $page_height));
$pdf->AddPage();
$pdf->SetAutoPageBreak(FALSE);
$pdf->SetFont($font, '', $font_size);

$pdf->Image($logo_image, $logo_x_position, $logo_y_position, $logo_width, $logo_height);

$pdf->SetXY($customer_name_part_x, $customer_name_part_y);
$pdf->MultiCell($customer_name_part_width, $customer_name_part_row_height, $customer_name_part_text,
0, $customer_name_part_align);

$pdf->SetXY($paragraph_1_x, $paragraph_1_y);
$pdf->MultiCell($paragraph_1_width, $paragraph_1_row_height, $paragraph_1_text,
0, $paragraph_1_align);


$pdf->Output();

mysqli_close($conn);
?>