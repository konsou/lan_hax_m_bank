<?php
date_default_timezone_set("Europe/Helsinki");

require_once('logging.php');


function get_user_for_session($conn, int $session_id){
    if ($session_id == 0){ return 0; }

    $query = "SELECT user_id FROM sessions WHERE id = '{$session_id}'";
    $result = mysqli_query($conn, $query);
    $arr = mysqli_fetch_array($result);

    if ($arr == null){ return 0; }
    return intval($arr[0]);
}


function valid_session_active($conn, int $user_id){
    // 1 - Check if there's a session id saved to a cookie
    if (isset($_COOKIE['m_bank_session_id'])){
        $session_id = intval($_COOKIE['m_bank_session_id']);
        // 2 - check if given session is valid for current user
        if (session_valid($conn, $session_id)){
            // Session valid - login ok
            logging(DEBUG, "Session {$session_id} valid - continue ");
            return true;
        }
    }
    logging(DEBUG, "No valid session active");
    return false;
}

    
function session_valid($conn, int $session_id){
    // Checks if given session_id is valid
    // Return true/false

    // Session id 0 means invalid session
    if ($session_id == 0){ return false; }

    $query = "SELECT COUNT(*) FROM sessions WHERE id = '{$session_id}' AND active = '1'";
    //logging(DEBUG, "In session_valid - query: {$query}");
    $result = mysqli_query($conn, $query);
    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);

    if ($row['COUNT(*)'] == 1){ return true; }
    
    return false;
}


function add_session($conn, int $user_id){
    // Return the new session id or 0 if there was an error

    // TODO: SESSIONS SHOULD BE VALID ONLY FOR A SPECIFIC TIME - ENFORCED SERVER-SIDE!

    // Generate random session ID
    $session_id = rand();

    logging(INFO, "Adding new session for user {$user_id}...");

    // Add new session into database
    // TODO: THIS *WILL* FAIL WHEN THERE'S ALREADY AN ENTRY WITH THE SAME ID
    $query = "INSERT INTO sessions (id, user_id, active) VALUES ('{$session_id}', '{$user_id}', '1')";
    // echo "<br>In add_session - query: {$query} ";
    if (!mysqli_query($conn, $query)){
        // error msg
        logging(ERROR, "Can't add session {$session_id} - retrying might help?");
        return 0;
    }

    // Save session id to cookie
    $valid = time() + 3600 * 8; // 8 hours
    setcookie('m_bank_session_id', $session_id, $valid);

    logging(DEBUG, "Session id created: {$session_id}");
    return $session_id;
}


function logout($conn, int $session_id){
    logging(INFO, "Logging out - closing session {$session_id}");
    // End session
    $query = "UPDATE sessions SET active = '0' WHERE id = '{$session_id}'";
    $result = mysqli_query($conn, $query);

    // Delete cookie
    setcookie('m_bank_session_id', '0', 0);
}


?>