<?php
date_default_timezone_set("Europe/Helsinki");


function redirect_with_post_data($redirect_page, array $post_data){
    $post_data_string = "";
    foreach ($post_data as $key => $value){
        $post_data_string .= "<input type='hidden' name='{$key}' value='{$value}'>";
    }
    
    // Dirty redirection with POST data hack
    echo "
    <form id='redirect_form' method='post' action='{$redirect_page}'>
        {$post_data_string}
    </form>
    <script type='text/javascript'>
        document.getElementById('redirect_form').submit();
    </script>
    ";
}


function navigation_button($action, $text){
    // Return a string containing html code for a navigation button to change page
    return "<a href='#' onclick='javascript:post(\"banking.php\", { action: \"{$action}\" })'>{$text}</a>
    ";
}

?>