<?php

require_once 'mobile_detect/Mobile_Detect.php';


function snippet_page_start(){
    // Page start, head tag and start of body tag

    $detect = new Mobile_Detect;
    if ( $detect->isMobile() ) { $mobile_css_line = "<link href='css/style_mobile.css' rel='stylesheet' type='text/css' />"; }
    else { $mobile_css_line = ""; }

    return "<!DOCTYPE html>
<html>
    <head>
        <title>M-Pankki - asiakkaan pankki!</title>
        <link rel='shortcut icon' href='img/logo_mbank_round.svg' />
        <link href='css/niramit.css' rel='stylesheet'>
        <link href='css/font_instruction.css' rel='stylesheet' />
        <link href='css/style.css' rel='stylesheet' type='text/css' />
        {$mobile_css_line}
        <link href='css/style_datepicker.css' rel='stylesheet' type='text/css' />
        <script src='js/jquery-3.3.1.min.js'></script>
        <script src='js/jquery-ui-1.12.1/jquery-ui.js'></script>
        <script src='js/scripts.js'></script>
    </head>
    <body>
        <div id='page-container'>
            <img class='page-top-logo' src='img/logo_mbank.svg' alt='M-Pankin logo' />
            <h1 class='page-top-logo'>M-Pankki</h1>
    ";
}


function snippet_page_end(){
    // End tags for body and html
    return "
        <div id='footer'>
            Asiakaspalvelu: <a href='mailto:asiakaspalvelu@m-pankki.ml'>asiakaspalvelu@m-pankki.ml</a>
        </div>
    </div>
</body>
</html>
    ";
}


function snippet_login_form(){
    return "
    <div class='rounded-corners-1'>
        <form name='login_form' action='banking.php' method='post'>
            <p>
                Käyttäjätunnus: <br />
                <input id='focus' name='login_form_user_id' type='text' />
            </p>
            <p>
                Salasana: <br />
                <input name='login_form_password' type='password' />
            </p>
            <input name='submit' type='submit' value='Lähetä' />
        </form>
    </div>
    ";
}

?>