<?php
date_default_timezone_set("Europe/Helsinki");

require_once('logging.php');


function key_list_id_for_user($conn, int $user_id){
    // 0 as return value means no key list for given user
    $query = "SELECT id FROM 2fa_lists WHERE user='{$user_id}' LIMIT 1";
    //logging(DEBUG, $query);
    $result = mysqli_query($conn, $query);
    if (!$result){ return 0; }
    $row = mysqli_fetch_array($result);
    if ($row == null){ return 0; }

    if (key_exists('id', $row)){ return $row['id']; }
    return 0;
}


function user_full_name($conn, int $user_id){
    $query = "SELECT display_name FROM users WHERE id = '{$user_id}'";
    $result = mysqli_query($conn, $query);
    if (!$result){ 
        logging(ERROR, "Error querying database in user_full_name: {$query}");
        return "";
    }
    else {
        $row = mysqli_fetch_array($result);
        if ($row == null) {
            logging(WARNING, "In user_full_name - no results for user {$user_id}");
            return "";
        }    
        return $row[0];
    }
}


function user_full_info($conn, int $user_id){
    $query = "SELECT * FROM users WHERE id = '{$user_id}'";
    $result = mysqli_query($conn, $query);
    if (!$result){ 
        logging(ERROR, "Error querying database in user_full_info: {$query}");
        return "";
    }
    else {
        $row = mysqli_fetch_array($result);
        if ($row == null) {
            logging(WARNING, "In user_full_info - no results for user {$user_id}");
            return "";
        }    
        return $row;
    }
}


function get_all_users($conn, $user_id){
    // Only admin can list all users
    if (!is_admin($conn, $user_id)){
        $user_name = user_full_name($user_id);
        logging(WARNING, "user {$user_id} ({$user_name}) trying to list all users - not an admin");
        return array();
    }

    $query = "SELECT * FROM users";
    $result = mysqli_query($conn, $query);
    if (!$result){ 
        logging(ERROR, "error processing query in get_all_users");
        return array();
    }
    return mysqli_fetch_all($result, MYSQLI_BOTH);
}

?>