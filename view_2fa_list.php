<?php
date_default_timezone_set("Europe/Helsinki");

require_once('2fa_functions.php');
require_once('functions.php');

function construct_2fa_list_page($conn, int $user_id, int $list_id){
    $codes = get_2fa_list($conn, $list_id);

    $rows = 5;
    $columns = 4;

    $output = "
    <table>";

    for ($row = 1; $row <= $rows; $row++){
        $output .= "
        <tr>";

        for ($column = 1; $column <= $columns; $column++){
            $list_key = $row + ($rows * ($column - 1));

            $key_formatted = pad_with_zeroes($list_key, 2);
            $value_formatted = pad_with_zeroes($codes[$list_key - 1], 4);
            $output .= "
            <td>{$key_formatted}: {$value_formatted}</td>";
        }
        $output .= "
        </tr>";
    }
    return $output;
}
?>