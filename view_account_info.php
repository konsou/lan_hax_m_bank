<?php
date_default_timezone_set("Europe/Helsinki");

require_once('logging.php');
require_once('functions.php');
require_once('account.php');
require_once('user.php');


function construct_account_info_page($conn, int $user_id, string $account_number){
    $account_balance = nice_currency_format_html(account_balance($conn, $account_number, $user_id));
    $transactions = account_transactions($conn, $user_id, $account_number);

    $transactions_string = "
    <h2>Tilitapahtumat - tili {$account_number}</h2>
    <h2>Saldo:<br />{$account_balance}</h2>
    <table>
        <tr class='alternating-background'>
            <th>Aika</th>
            <th>Maksaja</th>
            <th>Saaja</th>
            <th>Summa</th>
            <th>Viesti</th>
        </tr>";

    
    
    // TODO: DOES CACHING EVEN WORK HERE?
    $cached_account_owner_ids = array();
    $cached_account_owner_names = array();


    foreach ($transactions as $transaction){
        //log_array("DEBUG", $transaction);
        $date_string = date("d.m.Y H:i:s", $transaction[5]);
        $from_account_number = $transaction[1];
        $to_account_number = $transaction[2];

        //info[0] Resolve user ids that own account numbers
        // With result caching to avoid zillions of queries
        if (key_exists($from_account_number, $cached_account_owner_ids)){
            $from_user_id = $cached_account_owner_ids[$from_account_number];
        }
        else {
            $from_user_id = account_owner($conn, $from_account_number);
            $cached_account_owner_ids[$from_account_number] = $from_user_id;
        }
        if (key_exists($to_account_number, $cached_account_owner_ids)){
            $to_user_id = $cached_account_owner_ids[$to_account_number];
        }
        else {
            $to_user_id = account_owner($conn, $to_account_number);
            $cached_account_owner_ids[$to_account_number] = $to_user_id;
        }

        // Resolve user names for user id's
        // With result caching to avoid zillions of queries
        if (key_exists($from_user_id, $cached_account_owner_names)){
            $from_account_name = $cached_account_owner_names[$from_user_id];
        }
        else {
            $from_account_name = user_full_name($conn, $from_user_id);
            $cached_account_owner_names[$from_user_id] = $from_account_name;
        }
        if (key_exists($to_user_id, $cached_account_owner_names)){
            $to_account_name = $cached_account_owner_names[$to_user_id];
        }
        else {
            $to_account_name = user_full_name($conn, $to_user_id);
            $cached_account_owner_names[$to_user_id] = $to_account_name;
        }

        $amount_string = number_format($transaction[3], 2, ',', '&nbsp;');
        
        if ($account_number == $transaction[1]) { $amount_string = "<span class='negative-amount'>-&nbsp;{$amount_string}&nbsp;€</span>"; }
        else { $amount_string = "+ {$amount_string} €"; }

        $transactions_string .= "
        <tr class='alternating-background'>
            <td>{$date_string}</td>
            <td>{$from_account_name} {$transaction[1]}</td>
            <td>{$to_account_name} {$transaction[2]}</td>
            <td class='contains-number'>{$amount_string}</td>
            <td>{$transaction[4]}</td>
        </tr>
        ";
    }
    $transactions_string .= "
    </table>";

    return $transactions_string;
}
?>