<?php
date_default_timezone_set("Europe/Helsinki");

require_once('site_navigation.php');
require_once('2fa_functions.php');
require_once('account.php');


function construct_admin_page($conn, int $user_id){
    // Returns a string that contains admin page stuff
    $unused_2fa_lists = unused_2fa_lists($conn);
    $add_more_2fa_lists_button = navigation_button("create_2fa_codes", "Luo lisää", $user_id);
    $add_transactions_button = navigation_button("admin_add_transactions", "Luo tilitapahtumia", $user_id);

    $page = "<h2>HALLINTASIVU</h2>
    {$add_transactions_button}
    
    <h3>Käyttämättömiä salasanalistoja: {$unused_2fa_lists}</h3>
    {$add_more_2fa_lists_button}

    <h3>Vahvistuskoodilistan näyttö</h3>
    <form name='navigation_button_view_2fa_list' action='banking.php' method='post'>
        Listan numero: <input type='text' name='list_id' />
        <input type='hidden' name='action' value='view_2fa_list' />
        <input type='hidden' name='user_id' value='{$user_id}' />
        <input type='submit' value='Näytä' />
    </form>
    
    ";

    return $page;

}


function construct_admin_add_transaction_page($conn, $user_id, array $transfer_info = null){
    $select_account_from = account_selection_html($conn, $user_id, "account_from", true);
    $select_account_to = account_selection_html($conn, $user_id, "account_to", true);

    if ($transfer_info == null) { $transfer_message = "<h2>Siirtoa ei tehty</h2>"; }
    else {
        $transfer_date = timestamp_to_date($transfer_info[0]);
        $transfer_message = "<h2>Siirto onnistui</h2>
        <table>
            <tr>
                <th>Päiväys:</th>
                <td>{$transfer_date}</td>
            </tr>
            <tr>
                <th>Tililtä:</th>
                <td>{$transfer_info[1]}</td>
            </tr>
            <tr>
                <th>Tilille:</th>
                <td>{$transfer_info[2]}</td>
            </tr>
            <tr>
                <th>Summa:</th>
                <td>{$transfer_info[3]}</td>
            </tr>
            <tr>
                <th>Viesti:</th>
                <td>{$transfer_info[4]}</td>
            </tr>
        </table>
            ";
    }
    

    $page = "{$transfer_message}
    <h2>Lisää tilitapahtuma</h2>
    <form name='admin-add-transaction' action='banking.php' method='post'>
        <table class='layout-only'>
            <tr>
                <th>Pvm:</th>
                <td><input type='text' id='datepicker' name='payment_timestamp' /></td>
            </tr>
            <tr>
                <th>Tililtä:</th>
                <td>{$select_account_from}</td>
            </tr>
            <tr>
                <th>Tilille:</th>
                <td>{$select_account_to}</td>
            </tr>
            <tr>
                <th>Summa:</th>
                <td><input name='amount' type='text' /></td>
            </tr>
            <tr>
                <th>Viesti:</th>
                <td><input name='payment_message' type='text' /></td>
            </tr>
            <tr>
                <td><input type='hidden' name='action' value='admin_add_transactions' </td>
                <td><input type='submit' value='Lähetä' /></td>
            </tr>
        </table>
    </form>
        ";

    return $page;

}


?>