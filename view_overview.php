<?php
date_default_timezone_set("Europe/Helsinki");

require_once('account.php');
require_once('functions.php');
require_once('user.php');

function construct_overview_page($conn, int $user_id){
    $account_list = get_accounts($conn, $user_id);
    //print_array($account_list);
    $account_list_string = "
    <div class=''>
        <table>
            <tr class='alternating-background'>
                <th>Tilinumero</th>
                <th>Omistaja</th>
                <th>Saldo</th>
            </tr>";

    foreach ($account_list as $account){
        $account_number = $account[0];
        $account_owner_user_id = $account[1];
        $account_owner_name = user_full_name($conn, $account_owner_user_id);
        $account_balance = nice_currency_format_html($account[2]);
        //print_array($account);
        $account_list_string .= "
            <tr class='alternating-background'>
                <td><a href='#' onclick='javascript:post(\"banking.php\", { action:\"account_info\", account_number:\"{$account_number}\" })'>{$account_number}</a></td>
                <td>{$account_owner_name}</td>
                <td class='contains-number'>{$account_balance}</td>
            </tr>";
    }

    $account_list_string .= "
        </table>
    </div>";
    
    return "
    <h2>Tilit</h2>
    {$account_list_string}
    ";
}
?>