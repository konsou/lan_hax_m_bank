<?php
date_default_timezone_set("Europe/Helsinki");

require_once('account.php');
require_once('functions.php');
require_once('logging.php');


function construct_payment_page($conn, int $user_id){
    // Construct "From account" selection list
    //$accounts = get_accounts($conn, $user_id);
    //print_array($accounts);
    //echo "<br>{$accounts}";

    /*
    $account_selection_input = "<select name='account_from'>";
    foreach ($accounts as $account){
        $account_number = $account[0];
        $account_balance = nice_currency_format_string($account[2]);

        $account_selection_input .= "
        <option value='{$account_number}'>{$account_number} | {$account_balance}</option>";
    }
    $account_selection_input .= "</select>";
    */
    $account_selection_input = account_selection_html($conn, $user_id, "account_from");

    return "
    <h2>Uusi maksu</h2>
    <form name='transaction' action='banking.php' method='post'>
    <div class='new_payment_container'>
        <div>Tililtä:</div>
        <div>{$account_selection_input}</div>
        <div>Tilille:</div>
        <div><input type='text' name='account_to' /></div>
        <div>Summa:</div>
        <div><input type='text' name='amount' /></div>
        <div>Viesti:</div>
        <div>
        <textarea name='payment_message' rows='5' cols='50'></textarea>
        </div>
        <div><input type='submit' value='Maksa' /></div>
    <input type='hidden' name='user_id' value='{$user_id}' />
    <input type='hidden' name='action' value='transaction_init' />
    </div>
    </form>
            ";
}


function construct_payment_finished_page($conn, $user_id, $data, bool $success){
    if ($success){ $title = "Maksu suoritettu"; }
    else { $title = "Maksu epäonnistui"; }

    // VERY TODO: PREVENT RE-PAYMENT BY RELOADING THIS PAGE!
    // TODO: FILTER POST DATA BEFORE SHOWING

    return "
    <h2>{$title}</h2>
    <h3>Maksun tiedot:</h3>
    <table>
        <tr>
            <th>Tililtä:</th>
            <td><a href='banking.php?action=account_info&account_number={$data['account_from']}'>{$data['account_from']}</a></td>
        </tr>
        <tr>
            <th>Tilille:</th>
            <td>{$data['account_to']}</td>
        </tr>
        <tr>
            <th>Summa:</th>
            <td>{$data['amount']}</td>
        </tr>
        <tr>
            <th>Viesti:</th>
            <td>{$data['payment_message']}</td>
        </tr>
    </table>
            ";
}
?>