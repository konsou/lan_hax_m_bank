<?php
date_default_timezone_set("Europe/Helsinki");


require_once('user.php');


function construct_user_info_page($conn, int $user_id) {
    $user_info = user_full_info($conn, $user_id);
    $user_is_admin = $user_info[2];
    $user_name = $user_info[3];
    $user_phone_number = $user_info[4];
    $user_email = $user_info[5];

    if ($user_is_admin){ 
        $admin_row = "
        <tr class='alternating-background'>
            <th>On pääkäyttäjä:</th>
            <td>kyllä</th>
        </tr>
        ";
    }
    else { $admin_row = ""; }

    return "
        <h2>Omat tietosi</h2>
        <table>
            <tr class='alternating-background'>
                <th>Nimi:</th>
                <td>{$user_name}</th>
            </tr>
            {$admin_row}
            <tr class='alternating-background'>
                <th>Puhelinnumero:</th>
                <td>{$user_phone_number}</th>
            </tr>
            <tr class='alternating-background'>
                <th>Sähköpostiosoite:</th>
                <td>{$user_email}</th>
            </tr>
        </table>
        ";
}
?>