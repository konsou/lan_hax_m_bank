<?php
date_default_timezone_set("Europe/Helsinki");


require_once('user.php');
require_once('admin.php');


function construct_user_management_page($conn, int $user_id) {
    $user_is_admin = is_admin($conn, $user_id);

    if ($user_is_admin){ 
        $all_users = get_all_users($conn, $user_id);

        $page = "
        <table>
            <tr class='alternating-background'>
                <th>Käyttäjätunnus</th>
                <th>Salasana</th>
                <th>Avainlista</th>
                <th>Admin</th>
                <th>Nimi</th>
                <th>Puhelinnumero</th>
                <th>Email</th>
                <th>Kirje</th>
            </tr>
        ";

        foreach ($all_users as $user){
            //log_array(DEBUG, $user);
            $list_id = key_list_id_for_user($conn, $user['id']);
            $page .= "
            <tr class='alternating-background'>
                <td>{$user['id']}</td>
                <td>{$user['password']}</td>
                <td>{$list_id}</td>
                <td>{$user['is_admin']}</td>
                <td>{$user['display_name']}</td>
                <td>{$user['phone_number']}</td>
                <td>{$user['email']}</td>
                <td><a href='print_welcome_letter.php?user_id={$user['id']}' target='_blank'>kirje</a></td>
            </tr>
            ";
        }

        $page .= "
        </table>";

        return $page;

    }
    else { return "Not admin - go away"; }

}
?>